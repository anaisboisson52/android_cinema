package com.example.cinema

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView


class DetailsSalleActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_salle)




        val salleName = intent.getStringExtra("salleName")
        val salleAddress1 = intent.getStringExtra("salleAddress1")
        val salleAddress2 = intent.getStringExtra("salleAddress2")
        val salleCity = intent.getStringExtra("salleCity")
        val salleDescription = intent.getStringExtra("salleDescription")
        val salleParkingInfo = intent.getStringExtra("salleParkingInfo")
        val sallePublicTransport = intent.getStringExtra("sallePublicTransport")

        showBack()

        setHeaderTitle(salleName)



        findViewById<TextView>(R.id.textViewSalleAddress1).text = salleAddress1
        findViewById<TextView>(R.id.textViewSalleAddress2).text = salleAddress2
        findViewById<TextView>(R.id.textViewSalleCity).text = salleCity
        findViewById<TextView>(R.id.textViewSalleDescription).text = salleDescription
        findViewById<TextView>(R.id.textViewSalleParkingInfo).text = salleParkingInfo
        findViewById<TextView>(R.id.textViewSalleTransport).text = sallePublicTransport
    }
}
