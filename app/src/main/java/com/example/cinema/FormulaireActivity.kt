package com.example.cinema

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject


class FormulaireActivity : BaseActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulaire)


        sharedPreferences = getSharedPreferences("user_account", Context.MODE_PRIVATE)

        val editNom=findViewById<EditText>(R.id.editTextNom)
        val editPrenom=findViewById<EditText>(R.id.editTextPrenom)
        val editEmail=findViewById<EditText>(R.id.editTextEmail)
        val editAdresse=findViewById<EditText>(R.id.editTextAdresse)
        val editCodePostal=findViewById<EditText>(R.id.editTextCodePostal)
        val editVille=findViewById<EditText>(R.id.editTextVille)
        val editCateDeFidelite=findViewById<EditText>(R.id.editTextCarteDeFidelite)


        val buttonCreate= findViewById<Button>(R.id.buttonCreer)

        // Charger les données sauvegardées dans les champs du formulaire
        editNom.setText(readSharedPref("nom"))
        editPrenom.setText(readSharedPref("prenom"))
        editEmail.setText(readSharedPref("email"))
        editAdresse.setText(readSharedPref("adresse"))
        editCodePostal.setText(readSharedPref("codePostal"))
        editCateDeFidelite.setText(readSharedPref("cateDeFidelite"))
        editVille.setText(readSharedPref("ville"))



        // Récupérer les données du QR code transmises via l'Intent
        val qrCodeData = intent.getStringExtra("qrCodeData")
        if (qrCodeData != null) {
            // Convertir les données JSON du QR code en objet JSONObject
            val qrJsonObject = JSONObject(qrCodeData)

            // Extraire les données et les insérer dans les champs du formulaire
            editNom.setText(qrJsonObject.getString("lastName"))
            editPrenom.setText(qrJsonObject.getString("firstName"))
            editEmail.setText(qrJsonObject.getString("email"))
            editAdresse.setText(qrJsonObject.getString("address"))
            editCodePostal.setText(qrJsonObject.getString("zipcode"))
            editVille.setText(qrJsonObject.getString("city"))
            editCateDeFidelite.setText(qrJsonObject.getString("cardRef"))

        }

        buttonCreate.setOnClickListener {

            // Sauvegarder les données du formulaire dans les préférences partagées
            writeSharedPref("nom", editNom.text.toString())
            writeSharedPref("prenom", editPrenom.text.toString())
            writeSharedPref("email", editEmail.text.toString())
            writeSharedPref("adresse", editAdresse.text.toString())
            writeSharedPref("codePostal", editCodePostal.text.toString())
            writeSharedPref("ville", editVille.text.toString())
            writeSharedPref("cateDeFidelite", editCateDeFidelite.text.toString())

            // Mettre à jour la valeur de hasAccount à true pour indiquer que l'utilisateur a un compte
            sharedPreferences.edit().putBoolean("hasAccount", true).apply()
            // Afficher un message de succès
            Toast.makeText(applicationContext, "Données enregistrées avec succès", Toast.LENGTH_SHORT).show()

            // Redirection vers LogoActivity
            val intent = Intent(this@FormulaireActivity, LogoActivity::class.java)
            intent.putExtra("nom", editNom.text.toString())
            intent.putExtra("prenom", editPrenom.text.toString())
            intent.putExtra("email", editEmail.text.toString())
            intent.putExtra("adresse", editAdresse.text.toString())
            intent.putExtra("codePostal", editCodePostal.text.toString())
            intent.putExtra("ville", editVille.text.toString())
            startActivity(intent)
        }
    }


    private fun writeSharedPref(key: String, value: String) {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun readSharedPref(key: String): String {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "").toString()
    }


}