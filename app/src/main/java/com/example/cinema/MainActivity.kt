package com.example.cinema

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator

class MainActivity : AppCompatActivity() {

    private lateinit var scannedContent: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonFormulaire = findViewById<Button>(R.id.buttonFormulaire)
        buttonFormulaire.setOnClickListener {
            val newIntent = Intent(application, FormulaireActivity::class.java)
            startActivity(newIntent)
        }

        val buttonScanner = findViewById<Button>(R.id.buttonScanner)
        buttonScanner.setOnClickListener {
            // Lancer le scan de QR code
            val integrator = IntentIntegrator(this@MainActivity)
            integrator.setPrompt("Scan QR code")
            integrator.setBeepEnabled(true)
            integrator.setOrientationLocked(true)
            integrator.initiateScan()
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Vérifier si le résultat provient de l'activité de scan
        if (requestCode == IntentIntegrator.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // Récupérer le contenu du scan depuis l'intent de résultat
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    // Si le contenu du scan est vide
                    scannedContent = "Scan annulé"
                } else {
                    // Stocker le contenu du scan
                    scannedContent = result.contents
                    // Lancer l'activité de formulaire en transmettant les données du QR code
                    val intent = Intent(this@MainActivity, FormulaireActivity::class.java)
                    intent.putExtra("qrCodeData", scannedContent)
                    startActivity(intent)
                }
            }
        }
    }
}
