package com.example.cinema

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class CompteActivity : BaseActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compte)
        showBack()
        setHeaderTitle(getString(R.string.txtCompte))

        sharedPreferences = getSharedPreferences("user_account", Context.MODE_PRIVATE)

        val editNom = findViewById<EditText>(R.id.editTextNom)
        val editPrenom = findViewById<EditText>(R.id.editTextPrenom)
        val editEmail = findViewById<EditText>(R.id.editTextEmail)
        val editAdresse = findViewById<EditText>(R.id.editTextAdresse)
        val editCodePostal = findViewById<EditText>(R.id.editTextCodePostal)
        val editVille = findViewById<EditText>(R.id.editTextVille)
        val buttonEnregistrer = findViewById<Button>(R.id.buttonEnregistrer)

        // les données sauvegardées s'affichent dans les EditText
        editNom.setText(readSharedPref("nom"))
        editPrenom.setText(readSharedPref("prenom"))
        editEmail.setText(readSharedPref("email"))
        editAdresse.setText(readSharedPref("adresse"))
        editCodePostal.setText(readSharedPref("codePostal"))
        editVille.setText(readSharedPref("ville"))

        buttonEnregistrer.setOnClickListener {
            // Enregistrer les nouvelles valeurs saisies
            writeSharedPref("nom", editNom.text.toString())
            writeSharedPref("prenom", editPrenom.text.toString())
            writeSharedPref("email", editEmail.text.toString())
            writeSharedPref("adresse", editAdresse.text.toString())
            writeSharedPref("codePostal", editCodePostal.text.toString())
            writeSharedPref("ville", editVille.text.toString())

            // Mettre à jour la valeur de hasAccount à true pour indiquer que l'utilisateur a un compte
            sharedPreferences.edit().putBoolean("hasAccount", true).apply()
            Toast.makeText(this, "Données enregistrées avec succès", Toast.LENGTH_SHORT).show()
        }
    }

    private fun writeSharedPref(key: String, value: String) {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun readSharedPref(key: String): String {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "") ?: ""
    }
}
