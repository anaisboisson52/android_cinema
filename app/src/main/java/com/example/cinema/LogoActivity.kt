package com.example.cinema


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class LogoActivity : BaseActivity() {

    val SalesFragment= com.example.cinema.SalesFragment.newInstance("","")
    val CarteFragment= com.example.cinema.CarteFragment.newInstance("","")
    val FilmsFragment= com.example.cinema.FilmsFragment.newInstance("","")
    val PanierFragment= com.example.cinema.PanierFragment.newInstance("","")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logo)

        setHeaderTitle(null, R.drawable.cinema_logo_splash)

        val qrCodeData = intent.getStringExtra("qrCodeData")
        if (!qrCodeData.isNullOrEmpty()) {
            // Affichez les données du QR code si elles sont disponibles
            Toast.makeText(this, "Contenu du QR code : $qrCodeData", Toast.LENGTH_SHORT).show()
        }


        val sales= findViewById<TextView>(R.id.textViewTab2)
        val carte= findViewById<TextView>(R.id.textViewTab3)
        val films= findViewById<TextView>(R.id.textViewTab1)
        val panier= findViewById<TextView>(R.id.textViewTab4)


        showFilms()

        panier.setOnClickListener {
            showPanier()
        }

        sales.setOnClickListener {
            showSales()
        }

        carte.setOnClickListener {
            showCarte()
        }

        films.setOnClickListener {
            showFilms()
        }



        val nom = intent.getStringExtra("nom")
        val prenom = intent.getStringExtra("prenom")
        val email = intent.getStringExtra("email")
        val adresse = intent.getStringExtra("adresse")
        val codePostal = intent.getStringExtra("codePostal")
        val ville = intent.getStringExtra("ville")



        val userLogoCompte = findViewById<ImageView>(R.id.imageUserLogo)
        userLogoCompte.visibility = View.VISIBLE
        userLogoCompte.setOnClickListener {
            val intent = Intent(this@LogoActivity, CompteActivity::class.java)
            intent.putExtra("nom", nom)
            intent.putExtra("prenom", prenom)
            intent.putExtra("email", email)
            intent.putExtra("adresse", adresse)
            intent.putExtra("codePostal", codePostal)
            intent.putExtra("ville", ville)


            startActivity(intent)
        }
    }


    fun showSales() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Sales")
        fragmentTra.replace(R.id.layoutContent, SalesFragment)
        fragmentTra.commit()
    }
    fun showCarte() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Carte")
        fragmentTra.replace(R.id.layoutContent, CarteFragment)
        fragmentTra.commit()
    }

    fun showFilms() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Films")
        fragmentTra.replace(R.id.layoutContent, FilmsFragment)
        fragmentTra.commit()
    }

    fun showPanier() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Panier")
        fragmentTra.replace(R.id.layoutContent, PanierFragment)
        fragmentTra.commit()

    }


    }
